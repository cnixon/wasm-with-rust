extern {
    fn my_alert(ptr: *const u8, number: u32);
}

#[no_mangle]
pub extern "C" fn hello_world() {
    unsafe {
        let x = b"Hello World!\0";
        my_alert(x as *const u8, 42);
    }
}


// Give javascript a means to deallocate our strings
// Pretty trivial given that it won't be heap allocated...
#[no_mangle]
pub extern "C" fn dealloc_str() {
}
